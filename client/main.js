import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import Chart from 'chart.js';
import { SensorData } from '/imports/api/SensorData';

import './main.html';
import './sensor/sensor.js';

Template.cardTest.onCreated(function cardTestOnCreated() {
    this.cardArray = new ReactiveArray(
    [{
        tab1: "tab1",
        tab2: "tab2"
    },{
        tab1: "tab3",
        tab2: "tab4"
    },{
        tab1: "tab5",
        tab2: "tab6"
    }]
    );
});

Template.cardTest.helpers({
    cards: function() {
        return Template.instance().cardArray.get();
    },
    loadedFunction: function() {
        $('ul.tabs').tabs();
        console.info('loadedFunction');
    }
});

Template.cardTest.events({
    'click .btn' (event, instance) {
        let newArray = {
            tab1: "tab" + Math.floor(Math.random() * 1000000),
            tab2: "tab" + Math.floor(Math.random() * 1000000)
        };
        instance.cardArray.push(newArray);
        $('ul.tabs').tabs();
    },
    'load .card' (event) {
        console.info('load');
    }
});

Template.hello.onCreated(function helloOnCreated() {
  // counter starts at 0
  this.counter = new ReactiveVar(0);
});

Template.hello.helpers({
  counter() {
    return Template.instance().counter.get();
  },
});

Template.hello.events({
  'click button'(event, instance) {
    // increment the counter when button is clicked
    instance.counter.set(instance.counter.get() + 1);
  },
});
// getKeysWithValue: function () {
//     let data = this.sensordata();
//     let keys = [];
//     for (prop in data) {
//       keys.push({key: prop, value: data[prop]});
//     }
//     return keys;
//   }
function getChartData(sensor) {
    let data = {
        datasets: {},
        labels: []
    };
    let allSensorData = SensorData.find({
        topic: sensor.title
    }, { limit: 500}).fetch();
    for(prop in allSensorData[0].payload) {
        data.datasets[prop] = {
                fill: false,
                lineTension: 0.1,
                backgroundColor: "rgba(75,192,192,0.4)",
                borderColor: "rgba(75,192,192,1)",
                borderCapStyle: 'butt',
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: 'miter',
                pointBorderColor: "rgba(75,192,192,1)",
                pointBackgroundColor: "#fff",
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: "rgba(75,192,192,1)",
                pointHoverBorderColor: "rgba(220,220,220,1)",
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 10,
                spanGaps: false
        };
        data.datasets[prop].label = prop;
        data.datasets[prop].data = [];
    }
    allSensorData.forEach((thingy) => {
        let thing = thingy.payload;
        for (prop in thing) {
          data.datasets[prop].data.push(thing[prop]);
        }
    });
    allSensorData.forEach((thingy) => {
        data.labels.push(thingy.time);
    });
    let shit = [];
    for (key in data.datasets) {
        shit.push(data.datasets[key]);
    }
    data.datasets = shit;
    return data;
}

Template.chart.onRendered(function chartOnRendered() {
    let name = "#" + this.data.canvasName();
    let data = getChartData(this.data);
	var ctx = $(name);
    console.log('data', data);
    var myLineChart = new Chart(ctx, {
        type: 'line',
        data: data
    });
});
