import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { SensorData } from '/imports/api/SensorData';
import R from 'ramda';

import './sensor.html';

const sensorsVar = new ReactiveArray();
const wsUriC = "ws://192.168.178.20:1880/ws/sensortag";

Template.sensortag.helpers({
  sensors: function() { 
    return sensorsVar.get(); 
  },
  sorted: function(list) {
    var sortByCheckedTitle = R.sortWith([
      R.descend(R.prop('isChecked')),
      R.ascend(R.prop('title'))
    ]);
    return sortByCheckedTitle(list);
  },
  loadedFunction: function() {
    $('ul.tabs').tabs();
  }
});

Template.sensor.helpers({
  getKeysWithValue: function () {
    let data = this.sensordata();
    let keys = [];
    for (prop in data) {
      keys.push({key: prop, value: data[prop]});
    }
    return keys;
  }
});

/**
  This function sets up the websockets to gather all the sensordata.
**/

function setupSocket() {
  var ws;
  ws = new WebSocket(wsUriC);
  ws.onmessage = (msg) => {
    var parsed = JSON.parse(msg.data);
    parsed.time = Math.round(new Date().getTime()/1000.0);
    SensorData.insert(parsed);
    // console.info('DB', SensorData.find().fetch());
    updateSensors(parsed);
  }
  ws.onopen = function() {
     Materialize.toast('Connected to ' + wsUriC, 3000, 'grey darken-4');
    console.log("connected to " + wsUriC);
  }
  ws.onclose = function() {
    Materialize.toast('Lost connection to ' + wsUriC, 3000, 'red darken-2');
    setTimeout(setupSocket, 1000);
  }
}
Template.sensortag.onCreated(() => {
  setupSocket();
});

Template.sensor.events({
  /*
    Event fires on a click on input element. This would toggle the active/inactive state.
  */
  'click input' : function(evt) {
    this.isChecked.set(evt.currentTarget.checked);
    localStorage.setItem(this.title, this.isChecked.get());
  }
});

/**

  Class: Sensor
  Arguments: 
    title: String, a title for your sensor. 
    data: an object with the data from your sensor. format as
    {
      name: value
    }

**/

function Sensor(title, data) {
  this.getSavedCheck = function() {
    let checked = localStorage.getItem(this.title);
    if (R.isNil(checked)) {
      return true;
    } else {
      return checked;
    }
  }
  this.title = title;
  this.sensorName = function() {
    let arr = this.title.split('/');
    return arr[arr.length-1];
  },
  this.chartName = function() {
    return this.sensorName() + 'Chart';
  },
  this.canvasName = function() {
    return this.sensorName() + 'Canvas';
  },
  this.sensorVar = new ReactiveVar(data);
  this.sensordata = function() { 
    return this.sensorVar.get(); 
  };

  this.isChecked = new ReactiveVar(this.getSavedCheck());
  this.isCheckedFunc = function() {
    return (this.isChecked.get() == true); 
  };
}

/**

  Updates the data of the sensor, or creates a new Sensor object and adds it to the sensors array.

**/

function updateSensors(data) {
  let s = sensorsVar.get();
  let i = R.findIndex(R.propEq('title', data.topic))(s);
  if (i > -1) {
    s[i].sensorVar.set(data.payload);
  } else {
    sensorsVar.push(new Sensor(data.topic, data.data));
  }
}